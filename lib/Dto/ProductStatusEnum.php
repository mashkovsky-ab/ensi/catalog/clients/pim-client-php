<?php
/**
 * ProductStatusEnum
 *
 * PHP version 5
 *
 * @category Class
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * Ensi PIM.
 *
 * PIM
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Ensi\PimClient\Dto;
use \Ensi\PimClient\ObjectSerializer;

/**
 * ProductStatusEnum Class Doc Comment
 *
 * @category Class
 * @description Доступные статусы товаров: * 1 - новый * 2 - согласован * 3 - отклонен * 4 - заблокирован * 5 - снят с продажи * 6 - удален
 * @package  Ensi\PimClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class ProductStatusEnum
{
    /**
     * Possible values of this enum
     */
    const NEW = 1;
    const AGREED = 2;
    const REJECTED = 3;
    const LOCKED = 4;
    const CLOSED = 5;
    const DELETED = 6;
    
    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues(): array
    {
        return [
            self::NEW,
            self::AGREED,
            self::REJECTED,
            self::LOCKED,
            self::CLOSED,
            self::DELETED,
        ];
    }
}



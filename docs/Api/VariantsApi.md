# Ensi\PimClient\VariantsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVariantGroup**](VariantsApi.md#createVariantGroup) | **POST** /products/variants | Запрос на создание новой склейки
[**deleteVariantGroup**](VariantsApi.md#deleteVariantGroup) | **DELETE** /products/variants/{id} | Запрос на удаление склейки
[**getVariantGroup**](VariantsApi.md#getVariantGroup) | **GET** /products/variants/{id} | Запрос на получение склейки товаров
[**patchVariantGroup**](VariantsApi.md#patchVariantGroup) | **PATCH** /products/variants/{id} | Запрос на обновление отдельных свойств склейки
[**replaceVariantGroup**](VariantsApi.md#replaceVariantGroup) | **PUT** /products/variants/{id} | Запрос на обновление данных склейки
[**searchVariantGroups**](VariantsApi.md#searchVariantGroups) | **POST** /products/variants:search | Поиск склеек, удовлетворяющих фильтру



## createVariantGroup

> \Ensi\PimClient\Dto\VariantGroupResponse createVariantGroup($create_variant_group_request)

Запрос на создание новой склейки

Запрос на создание новой склейки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\VariantsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_variant_group_request = new \Ensi\PimClient\Dto\CreateVariantGroupRequest(); // \Ensi\PimClient\Dto\CreateVariantGroupRequest | 

try {
    $result = $apiInstance->createVariantGroup($create_variant_group_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VariantsApi->createVariantGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_variant_group_request** | [**\Ensi\PimClient\Dto\CreateVariantGroupRequest**](../Model/CreateVariantGroupRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\VariantGroupResponse**](../Model/VariantGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVariantGroup

> \Ensi\PimClient\Dto\EmptyDataResponse deleteVariantGroup($id)

Запрос на удаление склейки

Запрос на удаление склейки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\VariantsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteVariantGroup($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VariantsApi->deleteVariantGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getVariantGroup

> \Ensi\PimClient\Dto\VariantGroupResponse getVariantGroup($id, $include)

Запрос на получение склейки товаров

Запрос на получение склейки товаров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\VariantsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getVariantGroup($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VariantsApi->getVariantGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\VariantGroupResponse**](../Model/VariantGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchVariantGroup

> \Ensi\PimClient\Dto\VariantGroupResponse patchVariantGroup($id, $patch_variant_group_request)

Запрос на обновление отдельных свойств склейки

Запрос на обновление отдельных свойств склейки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\VariantsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_variant_group_request = new \Ensi\PimClient\Dto\PatchVariantGroupRequest(); // \Ensi\PimClient\Dto\PatchVariantGroupRequest | 

try {
    $result = $apiInstance->patchVariantGroup($id, $patch_variant_group_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VariantsApi->patchVariantGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_variant_group_request** | [**\Ensi\PimClient\Dto\PatchVariantGroupRequest**](../Model/PatchVariantGroupRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\VariantGroupResponse**](../Model/VariantGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceVariantGroup

> \Ensi\PimClient\Dto\VariantGroupResponse replaceVariantGroup($id, $replace_variant_group_request)

Запрос на обновление данных склейки

Запрос на обновление данных склейки

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\VariantsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_variant_group_request = new \Ensi\PimClient\Dto\ReplaceVariantGroupRequest(); // \Ensi\PimClient\Dto\ReplaceVariantGroupRequest | 

try {
    $result = $apiInstance->replaceVariantGroup($id, $replace_variant_group_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VariantsApi->replaceVariantGroup: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_variant_group_request** | [**\Ensi\PimClient\Dto\ReplaceVariantGroupRequest**](../Model/ReplaceVariantGroupRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\VariantGroupResponse**](../Model/VariantGroupResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVariantGroups

> \Ensi\PimClient\Dto\SearchVariantGroupsResponse searchVariantGroups($search_variant_groups_request)

Поиск склеек, удовлетворяющих фильтру

Поиск склеек, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\VariantsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_variant_groups_request = new \Ensi\PimClient\Dto\SearchVariantGroupsRequest(); // \Ensi\PimClient\Dto\SearchVariantGroupsRequest | 

try {
    $result = $apiInstance->searchVariantGroups($search_variant_groups_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VariantsApi->searchVariantGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_variant_groups_request** | [**\Ensi\PimClient\Dto\SearchVariantGroupsRequest**](../Model/SearchVariantGroupsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchVariantGroupsResponse**](../Model/SearchVariantGroupsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


# Ensi\PimClient\EnumsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getMetricsCategories**](EnumsApi.md#getMetricsCategories) | **GET** /classifiers/enums/metrics-categories | Возвращает информацию о доступных метриках ваалидации товаров
[**getProductTypes**](EnumsApi.md#getProductTypes) | **GET** /classifiers/enums/product-types | Возвращает информацию о доступных типах товаров
[**getPropertyTypes**](EnumsApi.md#getPropertyTypes) | **GET** /classifiers/enums/property-types | Возвращает информацию о доступных типах атрибутов



## getMetricsCategories

> \Ensi\PimClient\Dto\StringEnumInfoResponse getMetricsCategories()

Возвращает информацию о доступных метриках ваалидации товаров

Возвращает информацию о доступных метриках валидации товаров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getMetricsCategories();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getMetricsCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\PimClient\Dto\StringEnumInfoResponse**](../Model/StringEnumInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductTypes

> \Ensi\PimClient\Dto\IntegerEnumInfoResponse getProductTypes()

Возвращает информацию о доступных типах товаров

Возвращает информацию о доступных типах товаров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getProductTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getProductTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\PimClient\Dto\IntegerEnumInfoResponse**](../Model/IntegerEnumInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getPropertyTypes

> \Ensi\PimClient\Dto\StringEnumInfoResponse getPropertyTypes()

Возвращает информацию о доступных типах атрибутов

Возвращает информацию о доступных типах атрибутов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getPropertyTypes();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getPropertyTypes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\PimClient\Dto\StringEnumInfoResponse**](../Model/StringEnumInfoResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


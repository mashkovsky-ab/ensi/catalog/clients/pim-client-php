# Ensi\PimClient\ProductsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createProduct**](ProductsApi.md#createProduct) | **POST** /products/products | Запрос на создание нового товара
[**deleteProduct**](ProductsApi.md#deleteProduct) | **DELETE** /products/products/{id} | Запрос на удаление товара
[**getProduct**](ProductsApi.md#getProduct) | **GET** /products/published/{id} | Запрос на получение опубликованной версии товара
[**getProductDraft**](ProductsApi.md#getProductDraft) | **GET** /products/products/{id} | Запрос на получение черновика товара
[**massDeleteProducts**](ProductsApi.md#massDeleteProducts) | **POST** /products/products:mass-delete | Массовое удаление товаров
[**patchProduct**](ProductsApi.md#patchProduct) | **PATCH** /products/products/{id} | Запрос на обновление отдельных свойств товара
[**patchProductAttributes**](ProductsApi.md#patchProductAttributes) | **PATCH** /products/products/{id}/attributes | Обновление только заданных атрибутов товара
[**patchProductImages**](ProductsApi.md#patchProductImages) | **PATCH** /products/products/{id}/images | Обновление только заданных картинок
[**preloadProductImage**](ProductsApi.md#preloadProductImage) | **POST** /products/products:preload-image | Загрузка картинки для товара или атрибута товара
[**replaceProduct**](ProductsApi.md#replaceProduct) | **PUT** /products/products/{id} | Запрос на обновление товара
[**replaceProductAttributes**](ProductsApi.md#replaceProductAttributes) | **PUT** /products/products/{id}/attributes | Замещение всех атрибутов товара
[**replaceProductImages**](ProductsApi.md#replaceProductImages) | **PUT** /products/products/{id}/images | Замещение всех картинок товара
[**searchProductDrafts**](ProductsApi.md#searchProductDrafts) | **POST** /products/products:search | Поиск черновиков товаров, удовлетворяющих фильтру
[**searchProducts**](ProductsApi.md#searchProducts) | **POST** /products/published:search | Поиск опубликованных товаров, удовлетворяющих фильтру



## createProduct

> \Ensi\PimClient\Dto\ProductDraftResponse createProduct($create_product_request)

Запрос на создание нового товара

Запрос на создание нового товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_product_request = new \Ensi\PimClient\Dto\CreateProductRequest(); // \Ensi\PimClient\Dto\CreateProductRequest | 

try {
    $result = $apiInstance->createProduct($create_product_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->createProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_product_request** | [**\Ensi\PimClient\Dto\CreateProductRequest**](../Model/CreateProductRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteProduct

> \Ensi\PimClient\Dto\EmptyDataResponse deleteProduct($id)

Запрос на удаление товара

Запрос на удаление товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteProduct($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->deleteProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProduct

> \Ensi\PimClient\Dto\ProductResponse getProduct($id, $include)

Запрос на получение опубликованной версии товара

Запрос на получение опубликованной версии товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProduct($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\ProductResponse**](../Model/ProductResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getProductDraft

> \Ensi\PimClient\Dto\ProductDraftResponse getProductDraft($id, $include)

Запрос на получение черновика товара

Запрос на получение черновика товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getProductDraft($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->getProductDraft: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDeleteProducts

> \Ensi\PimClient\Dto\EmptyDataResponse massDeleteProducts($request_body_mass_delete)

Массовое удаление товаров

Массовое удаление товаров

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_delete = new \Ensi\PimClient\Dto\RequestBodyMassDelete(); // \Ensi\PimClient\Dto\RequestBodyMassDelete | 

try {
    $result = $apiInstance->massDeleteProducts($request_body_mass_delete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->massDeleteProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_delete** | [**\Ensi\PimClient\Dto\RequestBodyMassDelete**](../Model/RequestBodyMassDelete.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProduct

> \Ensi\PimClient\Dto\ProductDraftResponse patchProduct($id, $patch_product_request)

Запрос на обновление отдельных свойств товара

Запрос на обновление отдельных свойств товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_product_request = new \Ensi\PimClient\Dto\PatchProductRequest(); // \Ensi\PimClient\Dto\PatchProductRequest | 

try {
    $result = $apiInstance->patchProduct($id, $patch_product_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_product_request** | [**\Ensi\PimClient\Dto\PatchProductRequest**](../Model/PatchProductRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductAttributes

> \Ensi\PimClient\Dto\ProductAttributesResponse patchProductAttributes($id, $patch_product_attributes_request)

Обновление только заданных атрибутов товара

Добавление новых и обновление заданных атрибутов. Не указанные в запросе атрибуты не изменяются.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_product_attributes_request = new \Ensi\PimClient\Dto\PatchProductAttributesRequest(); // \Ensi\PimClient\Dto\PatchProductAttributesRequest | 

try {
    $result = $apiInstance->patchProductAttributes($id, $patch_product_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchProductAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_product_attributes_request** | [**\Ensi\PimClient\Dto\PatchProductAttributesRequest**](../Model/PatchProductAttributesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductAttributesResponse**](../Model/ProductAttributesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchProductImages

> \Ensi\PimClient\Dto\ProductImagesResponse patchProductImages($id, $patch_product_images_request)

Обновление только заданных картинок

Добавление новых и обновление только заданных картинок. Не указанные в запросе не изменяются.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_product_images_request = new \Ensi\PimClient\Dto\PatchProductImagesRequest(); // \Ensi\PimClient\Dto\PatchProductImagesRequest | 

try {
    $result = $apiInstance->patchProductImages($id, $patch_product_images_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->patchProductImages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_product_images_request** | [**\Ensi\PimClient\Dto\PatchProductImagesRequest**](../Model/PatchProductImagesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductImagesResponse**](../Model/ProductImagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## preloadProductImage

> \Ensi\PimClient\Dto\PreloadFile preloadProductImage($file)

Загрузка картинки для товара или атрибута товара

Загрузка картинки для товара или атрибута товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->preloadProductImage($file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->preloadProductImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\PreloadFile**](../Model/PreloadFile.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProduct

> \Ensi\PimClient\Dto\ProductDraftResponse replaceProduct($id, $replace_product_request)

Запрос на обновление товара

Запрос на обновление товара

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_product_request = new \Ensi\PimClient\Dto\ReplaceProductRequest(); // \Ensi\PimClient\Dto\ReplaceProductRequest | 

try {
    $result = $apiInstance->replaceProduct($id, $replace_product_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->replaceProduct: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_product_request** | [**\Ensi\PimClient\Dto\ReplaceProductRequest**](../Model/ReplaceProductRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductDraftResponse**](../Model/ProductDraftResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductAttributes

> \Ensi\PimClient\Dto\ProductAttributesResponse replaceProductAttributes($id, $replace_product_attributes_request)

Замещение всех атрибутов товара

Обновление значений всех атрибутов товара. Не указанные в запросе будут удалены.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_product_attributes_request = new \Ensi\PimClient\Dto\ReplaceProductAttributesRequest(); // \Ensi\PimClient\Dto\ReplaceProductAttributesRequest | 

try {
    $result = $apiInstance->replaceProductAttributes($id, $replace_product_attributes_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->replaceProductAttributes: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_product_attributes_request** | [**\Ensi\PimClient\Dto\ReplaceProductAttributesRequest**](../Model/ReplaceProductAttributesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductAttributesResponse**](../Model/ProductAttributesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductImages

> \Ensi\PimClient\Dto\ProductImagesResponse replaceProductImages($id, $replace_product_images_request)

Замещение всех картинок товара

Обновление всех картинок товара. Не указанные в запросе будут удалены.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_product_images_request = new \Ensi\PimClient\Dto\ReplaceProductImagesRequest(); // \Ensi\PimClient\Dto\ReplaceProductImagesRequest | 

try {
    $result = $apiInstance->replaceProductImages($id, $replace_product_images_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->replaceProductImages: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_product_images_request** | [**\Ensi\PimClient\Dto\ReplaceProductImagesRequest**](../Model/ReplaceProductImagesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductImagesResponse**](../Model/ProductImagesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductDrafts

> \Ensi\PimClient\Dto\SearchProductDraftsResponse searchProductDrafts($search_product_drafts_request)

Поиск черновиков товаров, удовлетворяющих фильтру

Поиск черновиков товаров, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_drafts_request = new \Ensi\PimClient\Dto\SearchProductDraftsRequest(); // \Ensi\PimClient\Dto\SearchProductDraftsRequest | 

try {
    $result = $apiInstance->searchProductDrafts($search_product_drafts_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchProductDrafts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_drafts_request** | [**\Ensi\PimClient\Dto\SearchProductDraftsRequest**](../Model/SearchProductDraftsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductDraftsResponse**](../Model/SearchProductDraftsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProducts

> \Ensi\PimClient\Dto\SearchProductsResponse searchProducts($search_products_request)

Поиск опубликованных товаров, удовлетворяющих фильтру

Поиск опубликованных товаров, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_products_request = new \Ensi\PimClient\Dto\SearchProductsRequest(); // \Ensi\PimClient\Dto\SearchProductsRequest | 

try {
    $result = $apiInstance->searchProducts($search_products_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductsApi->searchProducts: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_products_request** | [**\Ensi\PimClient\Dto\SearchProductsRequest**](../Model/SearchProductsRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductsResponse**](../Model/SearchProductsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


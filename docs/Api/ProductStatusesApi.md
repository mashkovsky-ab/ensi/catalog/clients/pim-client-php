# Ensi\PimClient\ProductStatusesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductStatus**](ProductStatusesApi.md#getProductStatus) | **GET** /classifiers/product-statuses/{id} | Получение статуса по идентификатору
[**replaceProductStatus**](ProductStatusesApi.md#replaceProductStatus) | **PUT** /classifiers/product-statuses/{id} | Обновление данных статуса
[**searchProductStatuses**](ProductStatusesApi.md#searchProductStatuses) | **POST** /classifiers/product-statuses:search | Поиск статусов товаров, удовлетворяющих условиям отбора



## getProductStatus

> \Ensi\PimClient\Dto\ProductStatusResponse getProductStatus($id)

Получение статуса по идентификатору

Получение статуса по идентификатору

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->getProductStatus($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->getProductStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\ProductStatusResponse**](../Model/ProductStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceProductStatus

> \Ensi\PimClient\Dto\ProductStatusResponse replaceProductStatus($id, $replace_product_status_request)

Обновление данных статуса

Обновление данных статуса

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_product_status_request = new \Ensi\PimClient\Dto\ReplaceProductStatusRequest(); // \Ensi\PimClient\Dto\ReplaceProductStatusRequest | 

try {
    $result = $apiInstance->replaceProductStatus($id, $replace_product_status_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->replaceProductStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_product_status_request** | [**\Ensi\PimClient\Dto\ReplaceProductStatusRequest**](../Model/ReplaceProductStatusRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\ProductStatusResponse**](../Model/ProductStatusResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchProductStatuses

> \Ensi\PimClient\Dto\SearchProductStatusesResponse searchProductStatuses($search_product_statuses_request)

Поиск статусов товаров, удовлетворяющих условиям отбора

Поиск статусов товаров, удовлетворяющих условиям отбора

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\ProductStatusesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_product_statuses_request = new \Ensi\PimClient\Dto\SearchProductStatusesRequest(); // \Ensi\PimClient\Dto\SearchProductStatusesRequest | 

try {
    $result = $apiInstance->searchProductStatuses($search_product_statuses_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductStatusesApi->searchProductStatuses: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_product_statuses_request** | [**\Ensi\PimClient\Dto\SearchProductStatusesRequest**](../Model/SearchProductStatusesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchProductStatusesResponse**](../Model/SearchProductStatusesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


# Ensi\PimClient\CategoriesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**bindCategoryProperties**](CategoriesApi.md#bindCategoryProperties) | **POST** /categories/categories/{id}:bind-properties | Запрос на изменение привязанных к категории атрибутов
[**createCategory**](CategoriesApi.md#createCategory) | **POST** /categories/categories | Запрос на создание новой категории
[**deleteCategories**](CategoriesApi.md#deleteCategories) | **POST** /categories/categories:mass-delete | Массовое удаление категорий
[**deleteCategory**](CategoriesApi.md#deleteCategory) | **DELETE** /categories/categories/{id} | Запрос на удаление категории
[**deleteCategoryImage**](CategoriesApi.md#deleteCategoryImage) | **POST** /categories/categories/{id}:delete-image | Удаление текущей картинки категории
[**getCategoriesTree**](CategoriesApi.md#getCategoriesTree) | **POST** /categories/categories:tree | Формирование дерева категорий
[**getCategory**](CategoriesApi.md#getCategory) | **GET** /categories/categories/{id} | Запрос категории по ID
[**patchCategory**](CategoriesApi.md#patchCategory) | **PATCH** /categories/categories/{id} | Запрос на обновление отдельных полей категории
[**replaceCategory**](CategoriesApi.md#replaceCategory) | **PUT** /categories/categories/{id} | Запрос на обновление категории
[**searchCategories**](CategoriesApi.md#searchCategories) | **POST** /categories/categories:search | Поиск категорий, удовлетворяющих фильтру
[**uploadCategoryImage**](CategoriesApi.md#uploadCategoryImage) | **POST** /categories/categories/{id}:upload-image | Загрузка картинки для категории



## bindCategoryProperties

> \Ensi\PimClient\Dto\CategoryResponse bindCategoryProperties($id, $bind_category_properties_request)

Запрос на изменение привязанных к категории атрибутов

Запрос на изменение привязанных к категории атрибутов

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$bind_category_properties_request = new \Ensi\PimClient\Dto\BindCategoryPropertiesRequest(); // \Ensi\PimClient\Dto\BindCategoryPropertiesRequest | 

try {
    $result = $apiInstance->bindCategoryProperties($id, $bind_category_properties_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->bindCategoryProperties: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **bind_category_properties_request** | [**\Ensi\PimClient\Dto\BindCategoryPropertiesRequest**](../Model/BindCategoryPropertiesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## createCategory

> \Ensi\PimClient\Dto\CategoryResponse createCategory($create_category_request)

Запрос на создание новой категории

Запрос на создание новой категории

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_category_request = new \Ensi\PimClient\Dto\CreateCategoryRequest(); // \Ensi\PimClient\Dto\CreateCategoryRequest | 

try {
    $result = $apiInstance->createCategory($create_category_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->createCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_category_request** | [**\Ensi\PimClient\Dto\CreateCategoryRequest**](../Model/CreateCategoryRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCategories

> \Ensi\PimClient\Dto\EmptyDataResponse deleteCategories($request_body_mass_delete)

Массовое удаление категорий

Массовое удаление категорий

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$request_body_mass_delete = new \Ensi\PimClient\Dto\RequestBodyMassDelete(); // \Ensi\PimClient\Dto\RequestBodyMassDelete | 

try {
    $result = $apiInstance->deleteCategories($request_body_mass_delete);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->deleteCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body_mass_delete** | [**\Ensi\PimClient\Dto\RequestBodyMassDelete**](../Model/RequestBodyMassDelete.md)|  |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCategory

> \Ensi\PimClient\Dto\EmptyDataResponse deleteCategory($id)

Запрос на удаление категории

Запрос на удаление категории

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCategory($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->deleteCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteCategoryImage

> \Ensi\PimClient\Dto\CategoryResponse deleteCategoryImage($id)

Удаление текущей картинки категории

Удаление текущей картинки категории

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteCategoryImage($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->deleteCategoryImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCategoriesTree

> \Ensi\PimClient\Dto\CategoriesTreeResponse getCategoriesTree($categories_tree_request)

Формирование дерева категорий

Формирование дерева категорий

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$categories_tree_request = new \Ensi\PimClient\Dto\CategoriesTreeRequest(); // \Ensi\PimClient\Dto\CategoriesTreeRequest | 

try {
    $result = $apiInstance->getCategoriesTree($categories_tree_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->getCategoriesTree: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **categories_tree_request** | [**\Ensi\PimClient\Dto\CategoriesTreeRequest**](../Model/CategoriesTreeRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\CategoriesTreeResponse**](../Model/CategoriesTreeResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getCategory

> \Ensi\PimClient\Dto\CategoryResponse getCategory($id, $include)

Запрос категории по ID

Запрос категории по ID

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getCategory($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->getCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **include** | **string**| Связанные сущности для подгрузки, через запятую | [optional]

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchCategory

> \Ensi\PimClient\Dto\CategoryResponse patchCategory($id, $patch_category_request)

Запрос на обновление отдельных полей категории

Запрос на обновление отдельных полей категории

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_category_request = new \Ensi\PimClient\Dto\PatchCategoryRequest(); // \Ensi\PimClient\Dto\PatchCategoryRequest | 

try {
    $result = $apiInstance->patchCategory($id, $patch_category_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->patchCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_category_request** | [**\Ensi\PimClient\Dto\PatchCategoryRequest**](../Model/PatchCategoryRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## replaceCategory

> \Ensi\PimClient\Dto\CategoryResponse replaceCategory($id, $replace_category_request)

Запрос на обновление категории

Запрос на обновление категории

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$replace_category_request = new \Ensi\PimClient\Dto\ReplaceCategoryRequest(); // \Ensi\PimClient\Dto\ReplaceCategoryRequest | 

try {
    $result = $apiInstance->replaceCategory($id, $replace_category_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->replaceCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **replace_category_request** | [**\Ensi\PimClient\Dto\ReplaceCategoryRequest**](../Model/ReplaceCategoryRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchCategories

> \Ensi\PimClient\Dto\SearchCategoriesResponse searchCategories($search_categories_request)

Поиск категорий, удовлетворяющих фильтру

Поиск категорий, удовлетворяющих фильтру

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_categories_request = new \Ensi\PimClient\Dto\SearchCategoriesRequest(); // \Ensi\PimClient\Dto\SearchCategoriesRequest | 

try {
    $result = $apiInstance->searchCategories($search_categories_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->searchCategories: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_categories_request** | [**\Ensi\PimClient\Dto\SearchCategoriesRequest**](../Model/SearchCategoriesRequest.md)|  |

### Return type

[**\Ensi\PimClient\Dto\SearchCategoriesResponse**](../Model/SearchCategoriesResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## uploadCategoryImage

> \Ensi\PimClient\Dto\CategoryResponse uploadCategoryImage($id, $file)

Загрузка картинки для категории

Загрузка картинки для категории

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\PimClient\Api\CategoriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл

try {
    $result = $apiInstance->uploadCategoryImage($id, $file);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CategoriesApi->uploadCategoryImage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]

### Return type

[**\Ensi\PimClient\Dto\CategoryResponse**](../Model/CategoryResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


# # CreateProductRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_id** | **string** | Код товара | [optional] 
**category_id** | **int** | Идентификатор категории | [optional] 
**brand_id** | **int** | Идентификатор бренда | [optional] 
**name** | **string** | Название товара | [optional] 
**code** | **string** | Код товара (выводится в URL) | [optional] 
**description** | **string** | Описание товара | [optional] 
**type** | **int** | Тип товара из ProductTypeEnum | [optional] 
**allow_publish** | **bool** | Публикация разрешена | [optional] 
**vendor_code** | **string** | Артикул | [optional] 
**barcode** | **string** | Штрихкод (EAN) | [optional] 
**weight** | **float** | Масса нетто, кг | [optional] 
**weight_gross** | **float** | Масса брутто, г | [optional] 
**length** | **float** | Длина, мм | [optional] 
**width** | **float** | Ширина, мм | [optional] 
**height** | **float** | Высота, мм | [optional] 
**is_adult** | **bool** | Товар 18+ | [optional] 
**images** | [**\Ensi\PimClient\Dto\EditProductImage[]**](EditProductImage.md) | Замещение всех имеющихся картинок на заданные | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



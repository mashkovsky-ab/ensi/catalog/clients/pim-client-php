# # VariantGroupIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**products** | [**\Ensi\PimClient\Dto\Product[]**](Product.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\Property[]**](Property.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # VariantGroupFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product_ids** | **int[]** | Идентификаторы товаров | [optional] 
**attribute_ids** | **int[]** | Идентификаторы атрибутов | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



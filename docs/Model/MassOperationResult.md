# # MassOperationResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**processed** | **int[]** |  | [optional] 
**errors** | [**\Ensi\PimClient\Dto\MassOperationResultErrors[]**](MassOperationResultErrors.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # ProductIncludes

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brand** | [**\Ensi\PimClient\Dto\Brand**](Brand.md) |  | [optional] 
**category** | [**\Ensi\PimClient\Dto\Category**](Category.md) |  | [optional] 
**images** | [**\Ensi\PimClient\Dto\ProductImage[]**](ProductImage.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\ProductAttributeValue[]**](ProductAttributeValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



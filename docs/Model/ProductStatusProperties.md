# # ProductStatusProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status_id** | **int** | Идентификатор статуса товара из ProductStatus | [optional] 
**status_comment** | **string** | Комментарий к статусу товара | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



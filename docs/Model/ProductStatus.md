# # ProductStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор статуса, соответствует значению перечисления ProductStatusEnum | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**name** | **string** | Наименование статуса | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # ProductImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор картинки | [optional] 
**url** | **string** | URL картинки | [optional] 
**file** | [**\Ensi\PimClient\Dto\File**](File.md) |  | [optional] 
**name** | **string** | Наименование (краткое описание картинки) | [optional] 
**sort** | **int** | Порядок сортирковки в коллекции картинок | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # CategoryReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор категории | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**is_real_active** | **bool** | Признак активности с учетом иерархии | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



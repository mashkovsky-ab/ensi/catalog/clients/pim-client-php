# # ProductField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор поля | [optional] 
**code** | **string** | Символьный код поля | [optional] 
**edit_mask** | **int** | Биты из FieldSettingsMaskEnum, соответствующие доступным для редактирования свойствам | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**name** | **string** | Наименование поля товара | [optional] 
**is_required** | **bool** | Признак обязательности заполнения | [optional] 
**is_moderated** | **bool** | Признак модерируемого поля | [optional] 
**metrics_category** | **string** | Категория метрики товара - значение перечисления MetricsCategoryEnum или null | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



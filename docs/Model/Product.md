# # Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор товара | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 
**main_image** | **string** | URL основной картинки | 
**external_id** | **string** | Код товара | [optional] 
**category_id** | **int** | Идентификатор категории | [optional] 
**brand_id** | **int** | Идентификатор бренда | [optional] 
**name** | **string** | Название товара | [optional] 
**code** | **string** | Код товара (выводится в URL) | [optional] 
**description** | **string** | Описание товара | [optional] 
**type** | **int** | Тип товара из ProductTypeEnum | [optional] 
**allow_publish** | **bool** | Публикация разрешена | [optional] 
**vendor_code** | **string** | Артикул | [optional] 
**barcode** | **string** | Штрихкод (EAN) | [optional] 
**weight** | **float** | Масса нетто, кг | [optional] 
**weight_gross** | **float** | Масса брутто, г | [optional] 
**length** | **float** | Длина, мм | [optional] 
**width** | **float** | Ширина, мм | [optional] 
**height** | **float** | Высота, мм | [optional] 
**is_adult** | **bool** | Товар 18+ | [optional] 
**brand** | [**\Ensi\PimClient\Dto\Brand**](Brand.md) |  | [optional] 
**category** | [**\Ensi\PimClient\Dto\Category**](Category.md) |  | [optional] 
**images** | [**\Ensi\PimClient\Dto\ProductImage[]**](ProductImage.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\ProductAttributeValue[]**](ProductAttributeValue.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



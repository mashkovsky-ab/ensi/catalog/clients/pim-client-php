# # BindCategoryPropertiesRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**replace** | **bool** | Если true, все отсутствующие в запросе привязки будут удалены | [optional] [default to false]
**properties** | [**\Ensi\PimClient\Dto\BindCategoryPropertiesRequestProperties[]**](BindCategoryPropertiesRequestProperties.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # VariantGroup

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор склейки | [optional] 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания | [optional] 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | [optional] 
**product_ids** | **int[]** | Идентификаторы товаров | [optional] 
**attribute_ids** | **int[]** | Идентификаторы атрибутов | [optional] 
**products** | [**\Ensi\PimClient\Dto\Product[]**](Product.md) |  | [optional] 
**attributes** | [**\Ensi\PimClient\Dto\Property[]**](Property.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



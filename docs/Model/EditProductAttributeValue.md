# # EditProductAttributeValue

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_id** | **int** | Идентификатор атрибута | [optional] 
**value** | **string** | Значение элемента справочника (string|number|integer|boolean) | [optional] 
**name** | **string** | Название значения (например, имя цвета) | [optional] 
**directory_value_id** | **int** | Идентификатор значения справочника. Если установлен, то value и name задавать не нужно. | [optional] 
**preload_file_id** | **int** | Идентификатор предварительно загруженного файла. Допустим только для атрибутов типа IMAGE. Если установлен, то поля value и directory_value_id должны отсутствовать. Поле name может быть задано, например, для описания картинки. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # DirectoryValueReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор значения | [optional] 
**type** | **string** | Тип значения из перечисления PropertyTypeEnum | [optional] 
**file** | [**\Ensi\PimClient\Dto\File**](File.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



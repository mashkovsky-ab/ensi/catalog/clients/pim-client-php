# # SearchPropertiesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) атрибутов | [optional] 
**name** | **string** | Название или часть названия атрибута | [optional] 
**code** | **string** | Код(ы) атрибутов | [optional] 
**is_filterable** | **bool** | Атрибут доступен для фильтрации на витрине | [optional] 
**is_active** | **bool** | Атрибут активен и доступен для товаров | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



# # PublishedProductProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**main_image** | **string** | URL основной картинки | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



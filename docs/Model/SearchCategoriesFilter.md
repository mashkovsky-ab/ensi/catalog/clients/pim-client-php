# # SearchCategoriesFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) категории | [optional] 
**name** | **string** | Название или часть названия категории | [optional] 
**code** | **string** | Код(ы) категорий | [optional] 
**parent_id** | **int** | Идентификатор(ы) родительской категории | [optional] 
**is_real_active** | **bool** | Признак фактической активности категории с учетом иерархии | [optional] 
**is_root** | **bool** | Отбирать только категории верхнего уровня или наоборот только вложенные | [optional] 
**exclude_id** | **int** | Идентификатор(ы) исключаемой категории | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



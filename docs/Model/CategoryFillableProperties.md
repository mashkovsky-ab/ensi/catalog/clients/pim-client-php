# # CategoryFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название категории | [optional] 
**code** | **string** | Код категории | [optional] 
**parent_id** | **int** | Идентификатор родительской категории | [optional] 
**is_inherits_properties** | **bool** | Категория наследует атрибуты родительской | [optional] 
**is_active** | **bool** | Признак активности данной категории | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



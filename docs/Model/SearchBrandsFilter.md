# # SearchBrandsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор(ы) бренда | [optional] 
**name** | **string** | Название или часть названия бренда | [optional] 
**code** | **string** | Код(ы) бренда | [optional] 
**is_active** | **bool** | Активность бренда | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



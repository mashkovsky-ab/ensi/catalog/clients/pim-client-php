# # CategoriesTreeItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор категории | [optional] 
**name** | **string** | Название категории | [optional] 
**code** | **string** | Код категории | [optional] 
**children** | **object[]** | Дочерние категории, если есть | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



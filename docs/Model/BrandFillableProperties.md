# # BrandFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Название бренда | [optional] 
**is_active** | **bool** | Активность бренда | [optional] 
**code** | **string** | Код бренда | [optional] 
**description** | **string** | Описание бренда | [optional] 
**logo_url** | **string** | URL логотипа на внешнем сервере | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)



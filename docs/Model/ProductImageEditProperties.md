# # ProductImageEditProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор имеющейся картинки. Если задан, то url и preload_file_id игнорируются. | [optional] 
**url** | **string** | URL картинки. Если задан, будет добавлена новая внешняя картинка. | [optional] 
**preload_file_id** | **int** | Идентификатор предзагруженного ранее файла. Если задан, будет создана новая картинка из файла. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


